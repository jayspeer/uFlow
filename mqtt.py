# Manually translated modules from Node-RED

import paho.mqtt.client as mqtt
mqtt_connected = False
mqtt_topic = None
client = None

def mqtt_connect(address, port):
    global mqtt_connected
    global client
    client = mqtt.Client()
    client.connect(address, port, 60)
    mqtt_connected = True

def mqtt_disconnect():
    global mqtt_connected
    global client
    client.disconnect()
    mqtt_connected = False
    

def mqtt_set_topic(topic):
    global mqtt_topic
    mqtt_topic = topic

def mqtt_out(msg):
    # TO DO - check if connection is up
    global client
    global mqtt_client
    if mqtt_connected:
        topic = ""
        if msg.get('topic', None) == None:
            topic = mqtt_topic
        else:
            topic = msg["topic"]

        client.publish(topic, str(msg["payload"]))
    return None

def mqtt_subscribe():
    # TO DO
    pass
    
        
    
