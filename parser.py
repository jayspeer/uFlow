#!/usr/bin/python3

import json
from uflow import uFlow
from mqtt import *

functions = {}
switches = {}
flow = {}
flow_start = {}

# mqtt
connection = False
broker = None
port = None

def parse_flow(nr_flow):
    f = open(nr_flow, "r")
    raw_flow = json.load(f)

    for node in raw_flow:
        if node["type"] == "inject":
            global function
            def inject(msg):
                return msg
            
            msg = {}
            if node.get('topic', None) != None:
                msg['topic'] = node['topic']
            if node.get('payload', None) != None:
                msg['payload'] = node['payload']
                
            f_name = node.get('id')
            functions[f_name] = inject # Add function to list of functions
            flow_start[f_name] = [f_name, msg]

            for wire in node['wires']:
                # Does not support multiple cables out of single output for now
                # TO DO - support ↑
                flow[f_name] = wire[0]

                
        elif node["type"] == "switch":
            if len(node['rules']) != len(node['wires']):
                print("CRITICAL - Number of rules and possible paths is different!")
                exit(2)

            payload = node['property']
            rules = node['rules']
            wires = node['wires']
            def function(msg):
                for i in range(0, len(rules)):
                    test_type = rules[i]['t']
                    var = rules[i]['v']
                    var_type = rules[i]['vt']

                    if test_type == 'eq':
                        if msg['payload'] == var:
                            return wires[i][0]
                    elif test_type == 'neq':
                        if msg['payload'] != var:
                            return wires[i][0]
                    # TO DO - Rest of comparisons

            s_name = node['id']
            switches[s_name] = function


        elif node["type"] == "debug":
            f_name = node['id']
            def debug(msg):
                print(msg)
            functions[f_name] = debug

            
        # MQTT
        elif node["type"] == "mqtt-broker":
            # TO DO - support for multiple brokers, possibly implemented via dict of all brokers "name": "id" or something
            global connection
            global broker
            global port
            connection = True
            broker = node['broker']
            port = int(node['port'])

        elif node["type"] == "mqtt out":
            # TO DO - add broker id 
            f_name = node['id']
            functions[f_name] = mqtt_out
                        
            
    print("FUNCTIONS")
    print(functions)
    print("FLOW")
    print(flow)
    print("SWITCHES")
    print(switches)
    print("FLOWSTART")
    print(flow_start)


def uFlow(function, payload):

    print("\n-----------------")
    print("Starting new flow")
    print("-----------------\n")

    current = function

    msg = {}
    msg["payload"] = payload

    while current != None:
        print("Current: " + str(current))
        print("Message: " + str(msg))
        if current in functions:
            msg = functions[current](msg)
            if msg == None:
                current = None
            else:
                current = flow[current]
        else:
            current = switches[current](msg)
        

                
parse_flow("test1.json")


if connection == True:
    mqtt_connect(broker, port)


for start in flow_start:
    print("\n===========\n")

    fs = flow_start[start]

    for fun in functions:
        print(functions[fun](fs[1]))
    
    uFlow(fs[0], fs[1])

if connection == True:
    mqtt_disconnect()
