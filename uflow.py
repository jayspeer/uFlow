#!/usr/bin/python3

import paho.mqtt.client as mqtt
from mqtt import *

def lower(msg):
    print(msg["payload"])
    msg["payload"] = msg["payload"].lower()
    return msg

def s1(msg):
    if msg["payload"] == "stop":
        return "b"
    else:
        return "c"

def b(msg):
    print("Command detected")
    return None

def c(msg):
    msg["payload"] = len(msg["payload"])
    return msg

def d(msg):
    print("Number of letters is " + str(msg["payload"]))
    return None

def topic(msg):
    msg["topic"] = msg["payload"]
    return msg

              
functions = {"lower": lower, "b": b, "c": c, "d": d, "mqtt_out": mqtt_out}
switches = {"s1": s1}
flow = {"lower": "s1", "c": "d"}
flowStart = "lower"

# uFlow python engine
def uFlow(function, payload):

    print("\n-----------------")
    print("Starting new flow")
    print("-----------------\n")

    current = function

    msg = {}
    msg["payload"] = payload

    while current != None:
        print("Current: " + str(current))
        print("Message: " + str(msg))
        if current in functions:
            msg = functions[current](msg)
            if msg == None:
                current = None
            else:
                current = flow[current]
        else:
            current = switches[current](msg)
        
# uFlow test
uFlow("lower", "TESTtestTEST test")
uFlow("lower", "STOP")

mqtt_connect("192.168.0.150", 1883)
mqtt_set_topic("topic/1")

flow = {"lower": "c", "c": "mqtt_out"}
uFlow("lower", "TEST")

mqtt_disconnect()
